package com.webapp.app.handler;

import com.webapp.app.dto.CreateMessageDto;
import com.webapp.app.services.MessageService;
import com.webapp.app.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.socket.CloseStatus;
import org.springframework.web.socket.TextMessage;
import org.springframework.web.socket.WebSocketSession;
import org.springframework.web.socket.handler.TextWebSocketHandler;
import org.springframework.stereotype.Component;

import java.util.*;

@Component
public class WebSocketHandler extends TextWebSocketHandler {

    private final UserService userService;
    private final MessageService messageService;
    private final Map<String, WebSocketSession> sessions = new HashMap<>();

    @Autowired
    public WebSocketHandler(UserService userService, MessageService messageService) {
        this.userService = userService;
        this.messageService = messageService;
    }

    private List<String> getUsernamesFromSession(WebSocketSession session) {
        String path = Objects.requireNonNull(session.getUri()).getPath();
        String[] pathSegments = path.split("/");
        String username1 = pathSegments[pathSegments.length - 2];
        String username2 = pathSegments[pathSegments.length - 1];

        List<String> usernames = new ArrayList<>();
        usernames.add(username1);
        usernames.add(username2);

        return usernames;
    }

    private String getUsernameFromTokenList(WebSocketSession session) {
        List<String> bearerTokenList = session.getHandshakeHeaders().get("token");
        assert bearerTokenList != null;
        // Get the token before connection established and control if the token username inside the ws link.
        String bearerToken = bearerTokenList.get(0);
        return userService.usernameByToken(bearerToken);
    }

    @Override
    public void afterConnectionEstablished(WebSocketSession session) throws Exception {
        String tokenUsername = getUsernameFromTokenList(session);

        List<String> usernames = getUsernamesFromSession(session);
        String username1 = usernames.get(0);
        String username2 = usernames.get(1);

        if (userService.isUserInConversation(tokenUsername, username1, username2)) {
            System.out.println("Connection established for user: " + username1 + " in conversation: " + username2);
            sessions.put(session.getId(), session);
        } else {
            System.out.println("User not authorized for this conversation");
            session.close(CloseStatus.NOT_ACCEPTABLE);
        }
    }

    @Override
    protected void handleTextMessage(WebSocketSession session, TextMessage message) throws Exception {
        String stringMessage = message.getPayload();
        System.out.println("Received message: " + stringMessage);

        String tokenUsername = getUsernameFromTokenList(session);
        List<String> usernames = getUsernamesFromSession(session);
        String username1 = usernames.get(0);
        String username2 = usernames.get(1);

        String senderUsername = "";
        String receiverUsername = "";

        if (tokenUsername.equals(username1)){
           senderUsername = username1;
           receiverUsername = username2;
        }
        else if (tokenUsername.equals(username2)){
           senderUsername = username2;
           receiverUsername = username1;
        }
        else {
            System.out.println("User not authorized for this conversation");
            session.close(CloseStatus.NOT_ACCEPTABLE);
        }

        // TODO Check the best solution.
        for (Map.Entry<String, WebSocketSession> sessionEntry : sessions.entrySet()) {
            if (Objects.equals(sessionEntry.getValue().getUri(), session.getUri())) {
                sessionEntry.getValue().sendMessage(new TextMessage(stringMessage));
            }
        }

        CreateMessageDto createMessageDto = new CreateMessageDto(senderUsername, receiverUsername, stringMessage);
        messageService.sendMessage(createMessageDto);
    }

    @Override
    public void afterConnectionClosed(WebSocketSession session, CloseStatus status) throws Exception {
        System.out.println("Connection closed: " + status);
    }
}
