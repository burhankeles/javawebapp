package com.webapp.app.services;

import com.webapp.app.config.auth.TokenService;
import com.webapp.app.entities.User;
import com.webapp.app.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
public class UserService implements UserDetailsService {

    private final UserRepository userRepository;
    private final TokenService tokenService;

    @Autowired
    public UserService(UserRepository userRepository, TokenService tokenService) {
        this.userRepository = userRepository;
        this.tokenService = tokenService;
    }

    public User loadUser(String login) {
        return (User) this.userRepository.findByLogin(login);
    }

    public List<String> getUsernames() {
        return userRepository.findAllLogin();
    }


    @Override
    public UserDetails loadUserByUsername(String login) {
      return this.userRepository.findByLogin(login);
    }


    // Websocket =========
    public String usernameByToken(String rawToken) {
        // We need to get token before connection established to control if the name of the writer and the request are available
        String token = rawToken.startsWith("Bearer ") ? rawToken.substring(7) : rawToken;
        String tokenUsername = tokenService.validateToken(token);
        System.out.println(tokenUsername);
        return tokenUsername;
    }

    public boolean isUserInConversation(String tokenUsername, String username1, String username2) {
        return tokenUsername.equals(username1) || tokenUsername.equals(username2);
    }
    // Websocket =========

}
