package com.webapp.app.services;

import com.webapp.app.dto.CreateMessageDto;
import com.webapp.app.model.MessageModel;
import com.webapp.app.dto.SenderReceiverDto;
import com.webapp.app.entities.Message;
import com.webapp.app.repositories.MessageRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;

@Service
public class MessageService {

  private final UserService userService;
  private final MessageRepository messageRepository;


  @Autowired
  public MessageService(UserService userService, MessageRepository messageRepository) {
    this.userService = userService;
    this.messageRepository = messageRepository;
  }

  public Message sendMessage(CreateMessageDto data) {
    Message message = new Message(data.message(), userService.loadUser(data.userSender()), userService.loadUser(data.userReceiver()), LocalDateTime.now());
    return this.messageRepository.save(message);
  }

  public List<MessageModel> getMessageBySenderAndReceiver(SenderReceiverDto sender_receiver, String tokenUsername) throws Exception {
    String sender = sender_receiver.userSender();
    String receiver = sender_receiver.userReceiver();

    // TODO Change this to a dedicated exception.
    if (!sender.equals(tokenUsername)) {
        throw new Exception("Sender does not match token username");
    }
    return messageRepository.findMessagesByUserSenderLoginAndUserReceiverLogin(sender, receiver);
  }
}
