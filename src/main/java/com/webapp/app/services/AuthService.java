package com.webapp.app.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import com.webapp.app.dto.SignUpDto;
import com.webapp.app.entities.User;
import com.webapp.app.exceptions.InvalidJwtException;
import com.webapp.app.repositories.UserRepository;

@Service
public class AuthService {


  private final UserRepository userRepository;

  @Autowired
  public AuthService(UserRepository userRepository) {
      this.userRepository = userRepository;
  }

  public UserDetails signUp(SignUpDto data) throws InvalidJwtException {

    if (this.userRepository.findByLogin(data.login()) != null) {
      throw new InvalidJwtException("Username already exists");
    }

    String encryptedPassword = new BCryptPasswordEncoder().encode(data.password());

    User newUser = new User(data.login(), encryptedPassword, data.role());

    return this.userRepository.save(newUser);

  }
}
