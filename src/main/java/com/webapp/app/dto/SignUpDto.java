package com.webapp.app.dto;

import com.webapp.app.enums.UserRole;

public record SignUpDto(
    String login,
    String password,
    UserRole role) {
}
