package com.webapp.app.dto;

public record SenderReceiverDto(
        String userSender,
        String userReceiver
) {
}
