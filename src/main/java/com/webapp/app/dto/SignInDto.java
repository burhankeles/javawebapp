package com.webapp.app.dto;

public record SignInDto(
    String login,
    String password) {
}

