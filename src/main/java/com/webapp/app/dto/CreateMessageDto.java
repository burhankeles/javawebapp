package com.webapp.app.dto;

public record CreateMessageDto(
        String userSender,
        String userReceiver,
        String message
) {
}
