package com.webapp.app.dto;

public record JwtDto(
    String accessToken) {
}
