package com.webapp.app.controllers;


import com.webapp.app.dto.CreateMessageDto;
import com.webapp.app.model.MessageModel;
import com.webapp.app.dto.SenderReceiverDto;
import com.webapp.app.services.MessageService;
import com.webapp.app.services.UserService;
import jakarta.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/v1/messages")
public class MessageController {
  private final MessageService messageService;
  private final UserService userService;

  @Autowired
  public MessageController(MessageService messageService, UserService userService) {
    this.messageService = messageService;
    this.userService = userService;
  }

  @PostMapping("/send_messages")
  public ResponseEntity<?> sendMessageController(@RequestBody @Valid CreateMessageDto data) {
    this.messageService.sendMessage(data);
    return ResponseEntity.status(HttpStatus.CREATED).build();
  }

  @GetMapping("/get_messages_by_sender_receiver")
  public ResponseEntity<?> getMessagesBySenderReceiver(@RequestHeader("Authorization") String bearerToken,
                                                       @RequestBody @Valid SenderReceiverDto data) {
    try {
      List<MessageModel> messageModels = this.messageService.getMessageBySenderAndReceiver(data, userService.usernameByToken(bearerToken));
      return ResponseEntity.ok(messageModels);
    }
    catch (Exception e) {
      System.out.println(e);
      return ResponseEntity.status(HttpStatus.FORBIDDEN).build();
    }

  }
}
