package com.webapp.app.controllers;

import com.webapp.app.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/v1/users")
public class UserController {
    private final UserService userService;
//    private final AuthService authService;

    @Autowired
    public UserController(UserService userService) {
        this.userService = userService;
//        this.authService = authService;
    }

    @GetMapping("/get_usernames")
    public ResponseEntity<?> getMessagesBySenderReceiver(){
        return ResponseEntity.ok(userService.getUsernames());
    }
}