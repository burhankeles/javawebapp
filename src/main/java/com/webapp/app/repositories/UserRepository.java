package com.webapp.app.repositories;

import jakarta.data.repository.Repository;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.security.core.userdetails.UserDetails;

import com.webapp.app.entities.User;

import java.util.List;

@Repository
public interface UserRepository extends JpaRepository<User, Long>{
  UserDetails findByLogin(String login);

  @Query("select login from users")
  List<String> findAllLogin();
}
