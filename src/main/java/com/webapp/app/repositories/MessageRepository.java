package com.webapp.app.repositories;

import com.webapp.app.model.MessageModel;
import com.webapp.app.entities.Message;
import jakarta.data.repository.Repository;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

@Repository
public interface MessageRepository extends JpaRepository<Message, Long> {
    // Add any custom query methods if needed
    List<MessageModel> findMessagesByUserSenderLoginAndUserReceiverLogin(String senderUsername, String receiverUsername);
}
