package com.webapp.app.entities;

import jakarta.persistence.*;
import lombok.*;

import java.time.LocalDateTime;

@Entity(name = "messages")
@Getter
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(of = "id")
public class Message {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String message;

    // From other tables
    @ManyToOne
    @JoinColumn(name = "user_id_sender")
    private User userSender;

    // From other tables
    @ManyToOne
    @JoinColumn(name = "user_id_receiver")
    private User userReceiver;

    private LocalDateTime timestamp;

    public Message(String message, User userSender, User userReceiver, LocalDateTime timestamp) {
        this.message = message;
        this.userSender = userSender;
        this.userReceiver = userReceiver;
        this.timestamp = timestamp;
    }

}
