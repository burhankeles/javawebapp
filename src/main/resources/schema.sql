CREATE TABLE if not exists users (
    id BIGSERIAL PRIMARY KEY,
    login VARCHAR(255) NOT NULL,
    password VARCHAR(255) NOT NULL,
    role VARCHAR(50) NOT NULL
);

CREATE TABLE if not exists messages (
    id BIGSERIAL PRIMARY KEY,
    message VARCHAR(255) NOT NULL,
    user_id_sender BIGINT,
    user_id_receiver BIGINT,
    timestamp TIMESTAMP,

    FOREIGN KEY (user_id_sender) REFERENCES users(id),
    FOREIGN KEY (user_id_receiver) REFERENCES users(id)
);