package com.webapp.app.controller;

import com.webapp.app.dto.CreateMessageDto;
import com.webapp.app.dto.SenderReceiverDto;
import com.webapp.app.dto.SignInDto;
import com.webapp.app.services.LoginTestService;
import com.webapp.app.services.WebSocketListener;
import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.server.LocalServerPort;
import org.testcontainers.containers.PostgreSQLContainer;

import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.WebSocket;

import static io.restassured.RestAssured.given;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class MessageControllerTest {

    private final LoginTestService loginTestService;

    // Check
    private String baseWsURI;
    private String username1;
    private String username2;
    private String accessToken;

    @Autowired
    public MessageControllerTest(LoginTestService loginTestService) {
        this.loginTestService = loginTestService;
    }

    @LocalServerPort
    private Integer port;

    static PostgreSQLContainer<?> postgres = new PostgreSQLContainer<>(
            "postgres:17"
    );

    @BeforeEach
    void setUp() {
        RestAssured.baseURI = "http://localhost:" + port;
        this.baseWsURI = "ws://localhost:" + port;
        this.username1 = "burhankeles";
        this.username2 = "burhankeles1";

        SignInDto user1 = new SignInDto("burhankeles", "mightydeveloper");
        this.accessToken = this.loginTestService.signIn(user1, 200);
    }

    @BeforeAll
    static void beforeAll() {
        postgres.start();
    }

    @AfterAll
    static void afterAll() {
        postgres.stop();
    }

    private String getWsTopic(String username1, String username2) {
        return baseWsURI + "/ws/" + username1 + "/" + username2;
    }

    @Test
    void shouldSendAMessage() throws Exception {
        String webSocketURI = getWsTopic(this.username1, this.username2);

        HttpClient client = HttpClient.newHttpClient();
        WebSocket webSocket = client.newWebSocketBuilder()
                .header("token", this.accessToken)
                .buildAsync(URI.create(webSocketURI), new WebSocketListener())
                .join();

        webSocket.sendText("Hello, WebSocket!", true);

        Thread.sleep(1000);
        // Sending a message
        assertFalse(webSocket.isInputClosed());

        // Waiting before closing
        Thread.sleep(1000);
        webSocket.sendClose(WebSocket.NORMAL_CLOSURE, "Goodbye").thenRun(() -> System.out.println("Closed"));
    }

    @Test
    void shouldNotSendAMessage() throws Exception {
        String webSocketURI = getWsTopic(this.username2, this.username2);

        HttpClient client = HttpClient.newHttpClient();
        WebSocket webSocket = client.newWebSocketBuilder()
                .header("token", this.accessToken)
                .buildAsync(URI.create(webSocketURI), new WebSocketListener())
                .join();

        webSocket.sendText("Hello, WebSocket!", true);

        Thread.sleep(1000);
        // Sending a message
        assertTrue(webSocket.isInputClosed());

        // Waiting before closing
        Thread.sleep(1000);
        webSocket.sendClose(WebSocket.NORMAL_CLOSURE, "Goodbye").thenRun(() -> System.out.println("Closed"));
    }

    private void saveMessage(CreateMessageDto message, String accessToken, int status){
        given()
           .contentType(ContentType.JSON)
           .header("Authorization", "Bearer " + accessToken) // Set the Authorization header
           .body(message)
           .when()
           .post("/api/v1/messages/send_messages") // Replace with your protected endpoint
           .then()
           .statusCode(status); // Check expected status code
    }

    @Test
    void shouldSaveMessage() {
        CreateMessageDto message1 = new CreateMessageDto(this.username1, this.username2, "Hi!");
        saveMessage(message1, this.accessToken, 201);
    }

//    TODO fix user in conversation
//    @Test
//    void shouldNotSaveMessage() {
//        CreateMessageDto message1 = new CreateMessageDto(this.username2, this.username2, "Hi!");
//        saveMessage(message1, this.accessToken, 403);
//    }

    private void getMessages(SenderReceiverDto senderReceiverDto, String accessToken, int status){
        given()
           .contentType(ContentType.JSON)
           .header("Authorization", "Bearer " + accessToken) // Set the Authorization header
           .body(senderReceiverDto)
           .when()
           .get("/api/v1/messages/get_messages_by_sender_receiver") // Replace with your protected endpoint
           .then()
           .statusCode(status);
    }

    @Test
    void shouldGetMessages(){
        SenderReceiverDto senderReceiverDto = new SenderReceiverDto(this.username1, this.username2);
        getMessages(senderReceiverDto, this.accessToken, 200);
    }

    @Test
    void shouldNotGetMessages(){
        SenderReceiverDto senderReceiverDto = new SenderReceiverDto(this.username2, this.username2);
        getMessages(senderReceiverDto, this.accessToken, 403);
    }

}



