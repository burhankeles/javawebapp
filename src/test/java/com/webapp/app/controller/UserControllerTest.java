package com.webapp.app.controller;

import com.webapp.app.dto.SignInDto;
import com.webapp.app.services.LoginTestService;
import io.restassured.RestAssured;
import io.restassured.http.ContentType;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.server.LocalServerPort;
import org.testcontainers.containers.PostgreSQLContainer;

import static io.restassured.RestAssured.given;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class UserControllerTest {

    private final LoginTestService loginTestService;
    private String accessToken;

    @Autowired
    public UserControllerTest(LoginTestService loginTestService) {
        this.loginTestService = loginTestService;
    }

    @LocalServerPort
    private Integer port;

    static PostgreSQLContainer<?> postgres = new PostgreSQLContainer<>(
            "postgres:17"
    );

    @BeforeEach
    void setUp() {
        RestAssured.baseURI = "http://localhost:" + port;
        SignInDto user1 = new SignInDto("burhankeles", "mightydeveloper");
        this.accessToken = this.loginTestService.signIn(user1, 200);
    }

    @BeforeAll
    static void beforeAll() {
        postgres.start();
    }

    @AfterAll
    static void afterAll() {
        postgres.stop();
    }

    private void getUsernames(String accessToken){
        given()
           .contentType(ContentType.JSON)
           .header("Authorization", "Bearer " + accessToken) // Set the Authorization header
           .when()
           .get("/api/v1/users/get_usernames") // Replace with your protected endpoint
           .then()
           .statusCode(200); // Check expected status code
    }

    @Test
    void shouldGetAllUsers(){
        getUsernames(this.accessToken);
    }
}
