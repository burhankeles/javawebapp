package com.webapp.app.controller;

import com.webapp.app.dto.SignInDto;
import com.webapp.app.dto.SignUpDto;
import com.webapp.app.enums.UserRole;
import com.webapp.app.repositories.UserRepository;
import com.webapp.app.repositories.MessageRepository;
import com.webapp.app.services.LoginTestService;
import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import org.junit.jupiter.api.*;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.server.LocalServerPort;
import org.testcontainers.containers.PostgreSQLContainer;

import static io.restassured.RestAssured.given;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class AuthControllerTest {

    private final LoginTestService loginTestService;

    @Autowired
    public AuthControllerTest(LoginTestService loginTestService) {
        this.loginTestService = loginTestService;
    }

    @LocalServerPort
    private Integer port;

    static PostgreSQLContainer<?> postgres = new PostgreSQLContainer<>(
            "postgres:17"
    );

    @BeforeEach
    void setUp() {
        RestAssured.baseURI = "http://localhost:" + port;
    }

    @BeforeAll
    static void beforeAll(@Autowired UserRepository userRepository, @Autowired MessageRepository messageRepository) {
        postgres.start();
        messageRepository.deleteAll();
        userRepository.deleteAll();
    }

    @AfterAll
    static void afterAll() {
        postgres.stop();
    }

    private void insertUser(SignUpDto user, int status) {
        given()
                .contentType(ContentType.JSON)
                .body(user)  // Pass the user object in the request body
                .when()
                .post("/api/v1/auth/signup")
                .then()
                .statusCode(status);
    }

    @Test
    @Order(1)
    void shouldRegisterNewUser() {
        UserRole userRoleUser = UserRole.USER;
        UserRole userRoleAdmin = UserRole.ADMIN;
        SignUpDto user1 = new SignUpDto("burhankeles", "mightydeveloper", userRoleUser);
        SignUpDto user2 = new SignUpDto("burhankeles1", "mightydeveloper", userRoleAdmin);

        insertUser(user1, 201);
        insertUser(user2, 201);
        insertUser(user1, 403);
    }

    @Test
    @Order(2)
    void shouldCheckNewUserLogin() {
        SignInDto user1 = new SignInDto("burhankeles", "mightydeveloper");
        SignInDto user2 = new SignInDto("burhankeles1", "mightydeveloper");
        SignInDto user3 = new SignInDto("burhankeles1", "mightydev");

        this.loginTestService.signIn(user1, 200);
        this.loginTestService.signIn(user2, 200);
        this.loginTestService.signIn(user3, 403);
    }
}