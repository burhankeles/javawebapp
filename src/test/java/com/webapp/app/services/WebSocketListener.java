package com.webapp.app.services;

import java.net.http.WebSocket;
import java.util.concurrent.CompletionStage;

// WebSocket Listener
public class WebSocketListener implements WebSocket.Listener {

    @Override
    public void onOpen(WebSocket webSocket) {
        System.out.println("WebSocket opened");
        webSocket.request(1);  // Request to receive next message
    }

    @Override
    public CompletionStage<?> onText(WebSocket webSocket, CharSequence data, boolean last) {
        System.out.println("Received message: " + data);
        webSocket.request(1);  // Request next message
        return null;
    }

    @Override
    public CompletionStage<String> onClose(WebSocket webSocket, int statusCode, String reason) {
        System.out.println("WebSocket closed with status " + statusCode + ", reason: " + reason);
        return null;
    }

    @Override
    public void onError(WebSocket webSocket, Throwable error) {
        System.out.println("Error occurred: " + error.getMessage());
    }
}