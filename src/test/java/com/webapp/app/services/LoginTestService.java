package com.webapp.app.services;

import com.webapp.app.dto.SignInDto;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import org.springframework.stereotype.Service;

import static io.restassured.RestAssured.given;

@Service
public class LoginTestService {
    public String signIn(SignInDto user, int status) {
        Response response = given()
                                .contentType(ContentType.JSON)
                                .body(user)
                                .when()
                                .post("/api/v1/auth/signin")
                                .then()
                                .statusCode(status)
                                .log()
                                .all()
                                .extract()
                                .response();
        if (status == 200) {
            return response.jsonPath().getString("accessToken");
        }
        return null;
    }
}
